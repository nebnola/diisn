Code for the Project "Delayed information in street networks" by Henrik Wolf and Friedrich Pagenkopf. Components:

* `streets2.py` for the simulation of the simple 2-street model
* `congestion_params.py` for running simulations with different parametres in bulk


For the rest of the files the purpose should be obvious from the filename
