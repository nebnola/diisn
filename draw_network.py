import time

import numpy as np
from PIL import ImageFont, Image, ImageDraw
from matplotlib import pyplot as plt

import analyse


def plot_network(env, t_index, heigth=400, width=400, padding=1.1, cmap=None, save_to_file=False, alternative_data=None,
                 comment="", draw_car_count = False, path=""):
    """returns image of the network state at time given by index t_index. The width of the lines between
    the nodes is a measure for the amount of cars currently on them. There are a few ways to adjust the output
    mainly by adjusting the variables below. The kwargs are used for some external control... no matter what you do. do not try to
    read this code. It is seriously messed up."""
    colors = plt.get_cmap('viridis').colors

    max_N = np.max(env.state)


    if cmap is None:
        def cmap(N):
            index = int(N / max_N * 256)
            index = 255 if index > 255 else index
            rgba = colors[index]
            return tuple(int(v * 256) for v in rgba)




    ################ overall settings ####################
    background_color = (255, 255, 255)
    font_size = 20
    headspace = 200

    # beauty settings
    circle_diameter = 40
    car_on_street_scale = 2
    edge_spacing = 5

    # colors
    circle_color = (74, 177, 255)
    circle_outline = (19, 114, 186)
    border_width_circle = 4

    line_there_color = (255, 187, 77)
    line_there_outline = (222, 140, 40)

    line_back_color = (174, 222, 98)
    line_back_outline = (120, 179, 48)
    border_width = 2

    ########## read till her. Do not venture deeper into this function. For the code is dark and treacherous.########
    try:
        font = ImageFont.truetype('arial.ttf', size=font_size)
    except Exception:
        font = ImageFont.load_default()

    # total_image
    total_height = int(heigth * padding)
    total_width = int(width * padding)
    total_image = Image.new("RGB", (total_width + headspace, total_height), background_color)

    # header
    header = Image.new("RGB", (headspace, heigth), background_color)
    header_draw = ImageDraw.Draw(header)
    titletext = f"time = {1 * t_index}\n" \
                f"Parameters:\n" \
                f"t_0 = {env.t_0}\n" \
                f"N_0 = {env.N_0}\n" \
                f"beta = {1}\n" \
                f"delay = {env.delay}\n" \
                f"r = {env.r}\n" \
                f"f: {env.f}\n" \
                f"{comment}"
    header_draw.multiline_text((0, 0), titletext, fill=(20, 20, 20), font=font)

    # main image
    im = Image.new("RGB", (heigth, width), background_color)
    draw = ImageDraw.Draw(im)

    scale = (heigth - circle_diameter) / (env.network.n_x - 1)

    if alternative_data is None:
        data_to_map = env.state[t_index]
    else:
        data_to_map = alternative_data

    # draw all edges between nodes
    for i, x in enumerate(data_to_map):
        try:  # if street is None, dont draw an edge
            start, end = env.network.get_coords_from_id(i)
        except ValueError:
            continue

        if cmap:
            line_there_color = line_back_color = cmap(x)
            line_back_outline = line_there_outline = line_there_color

        if i % 4 == 0:  # draw lanes going up
            start = np.array(start) * scale
            start[0] += circle_diameter / 2 + edge_spacing
            start[1] = heigth - start[1] - circle_diameter / 2
            end = np.array(end) * scale
            end[0] += circle_diameter / 2 + edge_spacing
            end[0] += 3 * car_on_street_scale
            end[1] = heigth - end[1] - circle_diameter / 2
            line_coords = list(start) + list(end)
            draw.rectangle(line_coords, fill=line_there_color, outline=line_there_outline, width=border_width)
            if draw_car_count: draw.multiline_text(((start[0]+end[0])/2, (start[1]+end[1])/2 - 30), str(x), fill=(20, 20, 20), font=font)


        elif i % 4 == 2:  # draw lanes going down
            start = np.array(start) * scale
            start[0] += circle_diameter / 2 - edge_spacing
            start[1] = heigth - start[1] - circle_diameter / 2
            end = np.array(end) * scale
            end[0] += circle_diameter / 2 - edge_spacing
            end[0] -= 3 * car_on_street_scale
            end[1] = heigth - end[1] - circle_diameter / 2
            line_coords = list(start) + list(end)
            draw.rectangle(line_coords, fill=line_back_color, outline=line_back_outline, width=border_width)
            if draw_car_count: draw.multiline_text(
                ((start[0] + end[0]) / 2, 30+(start[1] + end[1]) / 2), str(x),
                fill=(20, 20, 20), font=font)

        elif i % 4 == 3:  # draw lanes going right
            start = np.array(start) * scale
            start[0] += circle_diameter / 2
            start[1] = heigth - start[1] - circle_diameter / 2 + edge_spacing
            end = np.array(end) * scale
            end[0] += circle_diameter / 2
            end[1] = heigth - end[1] - circle_diameter / 2 + edge_spacing
            end[1] += 3 * car_on_street_scale
            line_coords = list(start) + list(end)
            draw.rectangle(line_coords, fill=line_there_color, outline=line_there_outline, width=border_width)
            if draw_car_count: draw.multiline_text(
                ((start[0] + end[0]) / 2 - 30, (start[1] + end[1]) / 2-10), str(x),
                fill=(20, 20, 20), font=font)

        elif i % 4 == 1:  # draw lanes going left
            start = np.array(start) * scale
            start[0] += circle_diameter / 2
            start[1] = heigth - start[1] - circle_diameter / 2 - edge_spacing
            end = np.array(end) * scale
            end[0] += circle_diameter / 2
            end[1] = heigth - end[1] - circle_diameter / 2 - edge_spacing
            end[1] -= 3 * car_on_street_scale
            line_coords = list(start) + list(end)
            draw.rectangle(line_coords, fill=line_back_color, outline=line_back_outline, width=border_width)
            if draw_car_count: draw.multiline_text(
                ((start[0] + end[0]) / 2+30, (start[1] + end[1]) / 2-10), str(x),
                fill=(20, 20, 20), font=font)

    # draw nodes over edges
    for i, x in enumerate(env.network.streets):
        bottom_left = env.network.get_point_from_pointid(i) * scale
        bottom_left[1] = heigth - bottom_left[1] - circle_diameter
        bounding_box = list(bottom_left) + list(bottom_left + circle_diameter)
        draw.ellipse(tuple(bounding_box), fill=circle_color, outline=circle_outline, width=border_width_circle)
        draw.multiline_text(bottom_left + circle_diameter / 4, str(i), fill=(20, 20, 20), font=font)

    # assemble final image
    total_image.paste(im, (int(headspace + (total_width - width) / 2), int((total_height - heigth) / 2)))
    header_horizontal = int((total_width - width) / 2)
    total_image.paste(header, (header_horizontal, int((total_height - heigth) / 2)))

    # save and return
    if save_to_file:
        total_image.save(f"{path}{t_index}_{time.time()}.png")
    return total_image


def save_anim_network(env, name="out", timestep=400, colormap=True):
    """saves an animated gif of the network attached to env."""
    if colormap:
        max_N = np.max(env.state)
        # if max_N > 20:
        #    max_N = 20

        colors = plt.get_cmap('viridis').colors

        def cmap(N):
            index = int(N / max_N * 256)
            index = 255 if index > 255 else index
            rgba = colors[index]
            return tuple(int(v * 256) for v in rgba)

    images = []
    for i in range(env.state.shape[0]):
        images.append(plot_network(env, i, cmap=cmap))
    images[0].save(f'{name}_{time.time()}.gif', save_all=True, append_images=images[1:],
                   duration=timestep * 1, loop=0)


def draw_car_distribution(env):
    cars = analyse.all_cars_streetwise(env)
    max_N = np.max(cars)
    min_N = np.min(cars[np.where(cars > 0)])
    print("max cars / min cars = ", max_N / min_N)
    colors = plt.get_cmap('viridis').colors

    def cmap(N):
        index = int(N / max_N * 256)
        index = 255 if index > 255 else index
        rgba = colors[index]
        return tuple(int(v * 256) for v in rgba)

    plot_network(env, 0, save_to_file=True, cmap=cmap, alternative_data=cars,
                 comment="_____________\nsum of all cars\non edges\nmax/min={:.2f}".format(max_N / min_N))
