import csv
import pickle
import re
import os
import numpy as np
import simulation

import network


def record_state(env, resolution=1):
    """ a simpy process to record the occupation numbers of all streets
    in regular intervals of size resolution
    """
    env.state_time_resolution = resolution
    streets = env.network.get_streets_flat()
    while True:
        yield env.timeout(resolution)
        current = np.zeros((1, len(streets)))
        for i, street in enumerate(streets):
            if street is not None:
                current[0, i] = street.N
        env.state = np.concatenate((env.state, current))


def store_state(env, file):
    """ stores the env in a pickled object
    """
    filename = os.path.join(file,f'r{env.r}delay{env.delay}f{env.f}'.replace('.','_'))
    dummyenv = simulation.DummyEnv(env)
    with open(filename, 'wb') as picklefile:
        pickle.dump(dummyenv, picklefile)


def load_state(filename, r, delay):
    """
    load the state of an env from filename
    The file consists of one or more csv parts, as outputted by store_state, separated by ---\nr:{r}, delay:{delay}
    Only choose the part where r and delay matches
    not used anymore
    """
    with open(filename) as file:
        filerows = iter(file)
        for line in filerows:
            if line == '----\n':
                argline = next(filerows)
                mr, mdelay = re.search(r'r=(\d+(?:\.\d+)?), delay=(\d+(?:\.\d+)?)', argline).groups()
                mr, mdelay = float(mr), float(mdelay)
                if r == mr and delay == mdelay:
                    break
        else:
            raise ValueError('No match found')

        rows = []
        reader = csv.reader(filerows)
        for row in reader:
            if row[0] == '----':  # end of the segment
                break
            rows.append(row)

    rows = np.asarray(rows, dtype=float)
    times = rows[:, 0]
    state = rows[:, 1:]

    class dummyEnv:
        pass

    env = dummyEnv()
    env.times = times
    env.state = state
    return env


def load_env(fn, r=None, delay=None, f=None, repetition=None):
    """ Load a pickled environment from storage.
    If r, delay, repetition are not given, interpret fn as a filename
    If they are given, fn is the directory in which a file is to be found according to the naming in congestion_params.py
    """
    if r is not None:
        fn = fn + '/' + f'r{r}delay{delay}f{f}rep{repetition}'.replace('.', '_')

    with open(fn, 'rb') as file:
        env = pickle.load(file)

    # add some stuff to the env so other functions work :)
    env.network = network.Network()
    env.state_time_resolution = env.times[1] - env.times[0]
    env.beta = 1

    return env
