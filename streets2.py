import simpy
import math
from numpy import random
import numpy as np
import matplotlib.pyplot as plt

class Street:
    def __init__(self, id, t_0, N_0):
        self.N = 1
        self.id = id
        self.t_0 = t_0
        self.N_0 = N_0

    def t(self):
        return self.t_0 * self.N_0 * (math.exp(self.N/self.N_0) - 1) / self.N


def store_state(env):
    now = [env.now, env.network[0].N, env.network[1].N]
    env.state.append(now)


class Car:
    def __init__(self, id, env, delay):
        self.env = env
        self.id = id
        self.delay = delay
        self.action = env.process(self.run())

    def run(self):
        times = np.array([street.t() for street in self.env.network])
        times = times - min(times) # substract a constant value from all times. This doesn't affect the
        # outcome because this results in a constant factor for the  probabilities which will be normalised
        # This helps avoid division by zero if p gets to small
        p = np.exp(-times)
        p = p/np.sum(p)
        #print(str0.N, str1.N, p0,p1)
        st = random.choice(self.env.network, p=p)
        
        yield self.env.timeout(self.delay)

        #print(f'Auto {self.id} nimmt strasse {str.id}')
        st.N+=1
        store_state(self.env)
        yield self.env.timeout(st.t())
        # wenn Auto angekommen ist
        st.N-=1
        store_state(self.env)
        #print(f'Auto {self.id} angekommen')
        return st


def spawn_car(env, r, delay):
    cars = []
    while True:
        dt = random.exponential(1/r)
        yield env.timeout(dt)
        cars.append(Car(len(cars),env, delay))

def do_sim(until=20,r=10, delay = 0):
    env = simpy.Environment()
    env.process(spawn_car(env,r, delay))
    env.network = [Street(1, 1,10), Street(2, 1,10)]
    env.state = [] # for recording what happens
    env.run(until=until)
    return env

def plot(env, ax=None, normalise = False):
    state = np.array(env.state)
    if ax is None:
        ax = plt.subplot(111)
    if normalise:
        nrm = 1/np.sum(state[:,1:], axis = 1)
        state[:,2] = state[:,2] * nrm
        state[:,1] = state[:,1] * nrm
    ax.plot(state[:,0],state[:,1],c='b')
    ax.plot(state[:,0],state[:,2],c='g')
    return ax


