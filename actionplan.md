## Was wir bisher haben

### 2-Straßen-Netzwerk

* Oszillationen gesehen
* Stau festgestellt bei verschiedenen Parametern
* Delay kann zu Überlastung führen

### 5x5-Grid

#### Für festes r, delay
* Simulation implementiert
* Animation
* Gesamtanzahl von Autos im System
* Aufschlüsselung nach inneren und äußeren Kanten
* real vs expected time
* Gesamtverteilung auf Straßen (hübsches Bild)

#### Für verschiedene r, delay
* avgtime(r, delay)
* Erkennung von Überlastung
* Mehrere Durchläufe


## Was man noch machen kann

### Analysen derselben Simulationen
* congestion Erkennung für mehrere Durchläufe (% congested)
* evtl längere Simulationslängen
* innere vs äußere Kanten für verschiedene delays
* real vs expected für verschiedene delays
* Fluss von ankommenden Autos?

### Andere Parameter
* f variieren
* all of the above für andere fs
* vergleich von delay mit f = 0
* Informationen mitteln
* verschiedene delays
