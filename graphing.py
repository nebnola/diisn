import csv

import matplotlib.pyplot as plt
import numpy as np

import analyse


def plot_avg(env, ax=None):
    T = analyse.avg_time(env)
    t = env.times
    if ax is None:
        ax = plt.subplot(111)
    ax.plot(t, T)
    return ax


def inner_outer(env, ax = None):
    res = analyse.inner_outer(env)
    t = res[:, 0]
    inr = res[:,1]
    outr = res[:,2]
    if ax is None:
        ax = plt.subplot(111)
    ax.plot(t, inr, label = 'inner')
    ax.plot(t, outr, label = 'outer')
    return ax


def total_cars(env, ax=None):
    """ Plot total # of cars in the system over time """
    res = analyse.total_cars(env)
    t, cars = res.T
    if ax is None:
        ax = plt.subplot(111)
    ax.plot(t, cars, label='total cars')
    return ax


def plot_real_time_vs_expected_time(env, ax = None):
    """plots the time needed per path against the time expected per path, this only considers
    cars which entered the network after the delay plus 1 timesteps"""
    _, t_p_exp = analyse.avg_expected_time(env, exclude_first=env.delay + 1)
    _, t_p_real = analyse.avg_real_time(env, exclude_first=env.delay + 1)

    # creates axis if not specified
    if ax is None:
        ax = plt.subplot(111, aspect=1)
    # draws id line, if there aren't already any lines in the axis object
    if not ax.lines:
        id = np.linspace(np.min(t_p_exp) - 0.02, np.max(t_p_real) + 0.02, 10)
        ax.plot(id, id, lw=1, c="orange", label="id")

    # add scatter plot with the points.
    ax.scatter(t_p_exp, t_p_real, label=f"delay = {env.delay}", marker=",", s=2)
    ax.set_ylabel("real travel time per edge")
    ax.set_xlabel("expected travel time per edge")
    ax.set_title("comparison of travel times for cars")
    ax.legend()
    return ax


def plot_multi_run_avg_time(envs, ax=None, label="", save=False):
    """draws the results of multiple runs of constant r into an axis"""
    if ax is None:
        ax = plt. subplot(111)
    avg_time = []
    delay = []
    for env in envs:
        delay.append(env.delay)
        real_times = analyse.avg_real_time(env, exclude_first=env.delay)[1]
        avg_time.append(np.mean(real_times))

    avg_time_sorted, delay_sorted = zip(*sorted(zip(avg_time, delay), key = lambda x: x[1]))
    avg_time_sorted = np.array(avg_time_sorted)
    delay_sorted = np.array(delay_sorted)
    delay_doubles = len(np.where(delay_sorted == delay_sorted[0])[0])
    time_reshape = np.reshape(avg_time_sorted, (-1, delay_doubles))
    data_list = [i for i in time_reshape]
    labels = np.unique(delay_sorted)
    ax.boxplot(data_list, labels=labels)
    ax.scatter(delay, avg_time, label=label)
    if save:
        writer = csv.writer(label)
        writer.writerows(zip(delay, avg_time))

    ax.set_xlabel("delay")
    ax.set_ylabel("avg time")
    ax.legend()
    return ax
